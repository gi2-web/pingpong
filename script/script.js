const circle = document.querySelector(".circle");
const rect = document.querySelector(".rect");
let cx = 0;
let cy = 0;
let sc = 0;

let rx = 0;
let ry = 0;
rect.addEventListener("keydown", moveRect);

function moveRect(e) {
    if(e.key == "ArrowRight") {
        rx += 10;
    }
    else if(e.key == "ArrowLeft") {
        rx -= 10;
    }
    updateRect();
}

function updateRect() {
    rect.style.left = rx + "px";
}